var mongoose = require("mongoose");
var mongoURL = process.env.OPENSHIFT_MONGODB_DB_PASSWORD ?  process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
		process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
		process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
		process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
		process.env.OPENSHIFT_APP_NAME
	  : '127.0.0.1:27017/app';
mongoose.connect(mongoURL);

module.exports.User = require('./user');