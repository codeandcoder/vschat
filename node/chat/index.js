var rooms = require('./rooms');
var users = require('./users');
var utils = require('../utils');

exports.userEnter = function(socket, callback) {
	users.userEnter(socket, function(err) {
		if (err) return callback(err);
		return callback(null);
	});
}

exports.sendMessage = function(socket, message) {
	var data = {
			user: {
				name: socket.user.name
			},
			message: utils.escapeHtml(message)
	}
	socket.broadcast.emit('message', data);
	socket.emit('message', data);
}

exports.userExit = function(socket) {
	users.userExit(socket, function(err) {
		if (err) return callback(err);
	});
}

exports.getUsers = function() {
	return users.getUsers();
}
