var users = {};

exports.userEnter = function(socket, callback) {
	if (users[socket.user.name] === undefined) {
		users[socket.user.name] = {
				user: {
					name: socket.user.name
				},
				sockets: {}
		}
		socket.broadcast.emit('user-enter', socket.user);
	}
	users[socket.user.name].sockets[socket.chatToken] = socket;
	return callback(null);
}

exports.userExit = function(socket, callback) {
	if (users[socket.user.name] !== undefined) {
		if (Object.keys(users[socket.user.name].sockets).length > 1) {
			delete users[socket.user.name].sockets[socket.chatToken];
		} else {
			delete users[socket.user.name];
			socket.broadcast.emit('user-exit', {
				name: socket.user.name
			});
		}
	}
	return callback(null);
}

exports.getUsers = function() {
	return Object.keys(users);
}