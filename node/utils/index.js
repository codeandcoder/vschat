// Token utils
var rand = function() {
    return Math.random().toString(36).substr(2);
};

exports.genToken = function() {
    return rand() + rand();
};

// Crypt utils
var crypto = require('crypto');
exports.cryptPass = function(pwd, salt, callback) {
  var hash = crypto.createHash('sha256').update(pwd + salt).digest('base64');
  callback(null, hash);
};

// Html utils
var entityMap = {
	    "&": "&amp;",
	    "<": "&lt;",
	    ">": "&gt;",
	    '"': '&quot;',
	    "'": '&#39;',
	    "/": '&#x2F;'
};

exports.escapeHtml = function (string) {
    return String(string).replace(/[&<>"'\/]/g, function (s) {
      return self.entityMap[s];
    });
}