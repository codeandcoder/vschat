var socket;

document.onkeydown = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == '13') {
        // Enter pressed
        sendMessage();
    }
}

window.onload = function() {
	socket = io.connect('http://localhost:3000');
	//socket = io.connect('http://vschat-candcoder.rhcloud.com:8000');
	
	socket.on('connect', function(data) {
		socket.emit('login');
	});
	
	socket.on('login-result', function(err, data) {
		if (err) {
			window.location = '/login'; 
		} else {
			for(var i = 0; i < data.users.length; i++) {
				insertUser(data.users[i]);
			}
		}
	});

	socket.on('user-enter', function(data) {
		insertUser(data.name);
	});
	
	socket.on('user-exit', function(data) {
		dropUser(data.name);
	});
	
	socket.on('message', function(data) {
		$('#messages').append('<div><span>' + data.user.name + '</span>: <span>' + data.message + '</span></div>');
	});
}

function insertUser(name) {
	$('#users').append('<div id="user-' + name + '">' + name + '</div>');
}

function dropUser(name) {
	$('#user-' + name).remove();
}

function sendMessage() {
	socket.emit('message', {message: $('#messageField').val()});
	$('#messageField').val('');
}