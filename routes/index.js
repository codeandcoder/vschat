exports.sessions = {};

var menu = [
            {'tag': 'Chat', 'ref':'#'}
            //{'tag': 'Crear Sala', 'ref':'/'},
            //{'tag': 'Contacto', 'ref':'/'}
];

var sidemenu = [
            {'tag': 'General'}
];

exports.index = function(req, res){
	exports.checkLogin(req.session.token, function(err) {
		if (err) {
			res.redirect('/login');
		} else {
			res.render('index', { title: 'VSChat 2.0', user: req.session.user,
				menu: menu, navbarActive: 'Chat',
				sidemenu: sidemenu, sideActive: 'General'});
		}
	})
};

exports.login = function(req, res){
	exports.checkLogin(req.session.token, function(err) {
		if (err) {
			res.render('login', { title: 'VSChat 2.0'});
		} else {
			res.redirect('/');
		}
	});
}

exports.logout = function(req, res){
	var sess = req.session;
	if (exports.sessions[sess.token] !== undefined) {
		delete exports.sessions[sess.token];
	}
	req.session.token = undefined;
	req.session.user = undefined;
	res.redirect('/login');
}

exports.register = function(req, res){
	exports.checkLogin(req.session.token, function(err) {
		if (err) {
			res.render('register', { title: 'VSChat 2.0'});
		} else {
			res.redirect('/');
		}
	});
}

exports.checkLogin = function(token, callback) {
	if (exports.sessions[token] !== undefined) {
		return callback(null);
	}
	return callback('expired');
}