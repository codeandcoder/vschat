
/**
 * Module dependencies.
 */

var express = require('express')
  , bodyParser = require('body-parser')
  , routes = require('./routes')
  , db = require('./node/db')
  , http = require('http')
  , path = require('path')
  , utils = require('./node/utils')
  , chat = require('./node/chat');

var app = express();

//all environments
var cookieParser = require('cookie-parser')();
var session = require('cookie-session')({ secret: 'secret' });
app.set('port', process.env.OPENSHIFT_NODEJS_PORT || 3000);
app.set('ip_addr', process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1');
app.set('views', __dirname + '/views');
app.locals.basedir = __dirname + '/public';
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});
app.use(cookieParser);
app.use(session);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(app.router);

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/login', routes.login);
app.get('/logout', routes.logout);
app.get('/register', routes.register);

//Login
app.post('/login',function(req,res){
	db.User.find({name: req.body.user}, function(err, users) {
	  if (err) throw err;
	  
	  if (users.length > 0) {
		  var user = users [0];
		  
		  utils.cryptPass(req.body.password, user.salt, function(err, hash) {
			  if (err) throw err;
			  
			  if (hash === user.password) {
				  var sessionToken = utils.genToken();
				  routes.sessions[sessionToken] = {user: user, time: new Date()};
				  req.session.user = user;
				  req.session.token = sessionToken;
				  res.redirect('/');
			  } else {
				  req.flash("alert alert-danger", "El login es incorrecto.", "");
				  res.redirect('/login');
			  }
		  });
	  } else {
		  req.flash("alert alert-danger", "El login es incorrecto.", "");
		  res.redirect('/login');
	  }
	});
});

// Register
app.post('/register',function(req,res){
	if (req.body.password !== req.body.confpassword) {
		req.flash("alert alert-danger", "¡Las contraseñas no coinciden!", "");
		res.redirect('/register');
	} else {
		db.User.find({name: req.body.user}, function(err, user) {
			if (err) throw err;
			
			if (user.length > 0) {
				req.flash("alert alert-danger", "El usuario %s ya existe", req.body.user);
				res.redirect('/register');
			} else {
				db.User.find({email: req.body.email}, function(err, user) {
					if (err) throw err;
					
					if (user.length > 0) {
						req.flash("alert alert-danger", "El email %s ya está registrado", req.body.email);
						res.redirect('/register');
					} else {
						var salt = utils.genToken();
						utils.cryptPass(req.body.password, salt, function(err, hash) {
							if (err) throw err;
							
							// create a new user
							var newUser = db.User({
							      email: req.body.email,
								  name: req.body.user,
								  password: hash,
								  salt: salt,
								  admin: false
							});
							// save the user
							newUser.save(function(err) {
							  if (err) throw err;

							  console.log('User created!');
							});
							var sessionToken = utils.genToken();
							routes.sessions[sessionToken] = {user: newUser, time: new Date()};
							req.session.user = newUser;
							req.session.token = sessionToken;
							res.redirect('/');
						});
					}
				});
			}
		});
	}
});

var server = http.createServer(app);
var io = require('socket.io')(server);

io.use(function(socket, next) {
    var req = socket.handshake;
    var res = {};
    cookieParser(req, res, function(err) {
        if (err) return next(err);
        session(req, res, next);
    });
});

server.listen(app.get('port'), app.get('ip_addr'), function(){
	console.log('Express server listening on port ' + app.get('port'));
});

io.on('connection', function(socket) {
	socket.on('disconnect', function(data) {
		if (socket.user !== undefined) {
			chat.userExit(socket, function(err) {
				if (err) throw err;
			});
		}
	});
	
	socket.on('login', function() {
		var token = socket.handshake.session.token;
		routes.checkLogin(token, function(err) {
			if (err) {
				socket.emit('login-result', err, null)
			} else {
				socket.chatToken = utils.genToken();
				socket.user = routes.sessions[token].user;
				
				chat.userEnter(socket, function(err) {
					if (err) {
						console.log(err);
						socket.emit('login-result', err, null);
					} else {
						socket.emit('login-result', err, {user: socket.user.name, users:chat.getUsers()});
					}
				});
			}
		})
	});
	
	socket.on('message', function(data) {
		var token = socket.handshake.session.token;
		routes.checkLogin(token, function(err) {
			if (err) {
				console.log(err);
			} else {
				chat.sendMessage(socket, data.message);
			}
		})
	});
});